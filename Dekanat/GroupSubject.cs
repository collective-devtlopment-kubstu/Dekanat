﻿using System;
using System.Collections.Generic;

namespace Dekanat;

public partial class GroupSubject
{
    public int IdGroup { get; set; }

    public int IdSubject { get; set; }

    public int IdTeacher { get; set; }

    public string Day { get; set; } = null!;

    public virtual Group IdGroupNavigation { get; set; } = null!;

    public virtual Subject IdSubjectNavigation { get; set; } = null!;

    public virtual Employee IdTeacherNavigation { get; set; } = null!;
}
