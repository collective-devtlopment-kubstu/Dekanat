﻿using System;
using System.Collections.Generic;

namespace Dekanat;

public partial class Department
{
    public int Id { get; set; }

    public int? IdInstitute { get; set; }

    public string? Name { get; set; }

    public virtual ICollection<Employee> Employees { get; } = new List<Employee>();

    public virtual Institute? IdInstituteNavigation { get; set; }

    public virtual ICollection<Specialization> Specializations { get; } = new List<Specialization>();
}
