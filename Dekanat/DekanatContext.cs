﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace Dekanat;

public partial class DekanatContext : DbContext
{
    private static DekanatContext? _context;

    public static DekanatContext GetContext()
    {
        if (_context == null)
            _context = new DekanatContext();

        return _context;
    }

    public DekanatContext()
    {
    }

    public DekanatContext(DbContextOptions<DekanatContext> options)
        : base(options)
    {
    }

    public virtual DbSet<Department> Departments { get; set; }

    public virtual DbSet<Employee> Employees { get; set; }

    public virtual DbSet<Group> Groups { get; set; }

    public virtual DbSet<GroupSubject> GroupSubjects { get; set; }

    public virtual DbSet<Institute> Institutes { get; set; }

    public virtual DbSet<Session> Sessions { get; set; }

    public virtual DbSet<Specialization> Specializations { get; set; }

    public virtual DbSet<Student> Students { get; set; }

    public virtual DbSet<Subject> Subjects { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        => optionsBuilder.UseNpgsql("Host=localhost;Port=5432;Database=Dekanat;Username=postgres;Password=13012003");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Department>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("departments_pkey");

            entity.ToTable("departments");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.IdInstitute).HasColumnName("id_institute");
            entity.Property(e => e.Name).HasColumnName("name");

            entity.HasOne(d => d.IdInstituteNavigation).WithMany(p => p.Departments)
                .HasForeignKey(d => d.IdInstitute)
                .HasConstraintName("departments_id_institute_fkey");
        });

        modelBuilder.Entity<Employee>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("employees_pkey");

            entity.ToTable("employees");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.Email).HasColumnName("email");
            entity.Property(e => e.IdDepartment).HasColumnName("id_department");
            entity.Property(e => e.Name).HasColumnName("name");
            entity.Property(e => e.PasData).HasColumnName("pas_data");
            entity.Property(e => e.Password).HasColumnName("password");
            entity.Property(e => e.Patronymic).HasColumnName("patronymic");
            entity.Property(e => e.Surname).HasColumnName("surname");
            entity.Property(e => e.Telephone).HasColumnName("telephone");

            entity.HasOne(d => d.IdDepartmentNavigation).WithMany(p => p.Employees)
                .HasForeignKey(d => d.IdDepartment)
                .HasConstraintName("employees_id_department_fkey");
        });

        modelBuilder.Entity<Group>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("groups_pkey");

            entity.ToTable("groups");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.IdSpecialization).HasColumnName("id_specialization");
            entity.Property(e => e.Name).HasColumnName("name");

            entity.HasOne(d => d.IdSpecializationNavigation).WithMany(p => p.Groups)
                .HasForeignKey(d => d.IdSpecialization)
                .HasConstraintName("groups_id_specialization_fkey");
        });

        modelBuilder.Entity<GroupSubject>(entity =>
        {
            entity.HasKey(e => new { e.IdGroup, e.IdSubject }).HasName("Group_Subject_pkey");

            entity.ToTable("Group_Subject");

            entity.Property(e => e.IdGroup).HasColumnName("ID_group");
            entity.Property(e => e.IdSubject).HasColumnName("ID_subject");
            entity.Property(e => e.IdTeacher).HasColumnName("ID_teacher");

            entity.HasOne(d => d.IdGroupNavigation).WithMany(p => p.GroupSubjects)
                .HasForeignKey(d => d.IdGroup)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("Group_Subject_ID_group_fkey");

            entity.HasOne(d => d.IdSubjectNavigation).WithMany(p => p.GroupSubjects)
                .HasForeignKey(d => d.IdSubject)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("Group_Subject_ID_subject_fkey");

            entity.HasOne(d => d.IdTeacherNavigation).WithMany(p => p.GroupSubjects)
                .HasForeignKey(d => d.IdTeacher)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("Group_Subject_ID_teacher_fkey");
        });

        modelBuilder.Entity<Institute>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("institute_pkey");

            entity.ToTable("institute");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.Address).HasColumnName("address");
            entity.Property(e => e.Name).HasColumnName("name");
        });

        modelBuilder.Entity<Session>(entity =>
        {
            entity
                .HasNoKey()
                .ToTable("sessions");

            entity.Property(e => e.Assessment).HasColumnName("assessment");
            entity.Property(e => e.Date).HasColumnName("date_");
            entity.Property(e => e.IdStudent).HasColumnName("id_student");
            entity.Property(e => e.IdSubject).HasColumnName("id_subject");

            entity.HasOne(d => d.IdStudentNavigation).WithMany()
                .HasForeignKey(d => d.IdStudent)
                .HasConstraintName("sessions_id_student_fkey");

            entity.HasOne(d => d.IdSubjectNavigation).WithMany()
                .HasForeignKey(d => d.IdSubject)
                .HasConstraintName("sessions_id_subject_fkey");
        });

        modelBuilder.Entity<Specialization>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("specializations_pkey");

            entity.ToTable("specializations");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.IdDepartment).HasColumnName("id_department");
            entity.Property(e => e.Name).HasColumnName("name");

            entity.HasOne(d => d.IdDepartmentNavigation).WithMany(p => p.Specializations)
                .HasForeignKey(d => d.IdDepartment)
                .HasConstraintName("specializations_id_department_fkey");
        });

        modelBuilder.Entity<Student>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("students_pkey");

            entity.ToTable("students");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.Email).HasColumnName("email");
            entity.Property(e => e.IdGroup).HasColumnName("id_group");
            entity.Property(e => e.Name).HasColumnName("name");
            entity.Property(e => e.PasData).HasColumnName("pas_data");
            entity.Property(e => e.Password).HasColumnName("password");
            entity.Property(e => e.Patronymic).HasColumnName("patronymic");
            entity.Property(e => e.Surname).HasColumnName("surname");
            entity.Property(e => e.Telephone).HasColumnName("telephone");

            entity.HasOne(d => d.IdGroupNavigation).WithMany(p => p.Students)
                .HasForeignKey(d => d.IdGroup)
                .HasConstraintName("students_id_group_fkey");
        });

        modelBuilder.Entity<Subject>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("subjects_pkey");

            entity.ToTable("subjects");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.Name).HasColumnName("name");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
