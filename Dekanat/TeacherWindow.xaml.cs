using System.Windows;

namespace Dekanat;

public partial class TeacherWindow : Window
{
    private Employee _teacher;
    public TeacherWindow( Employee teacher)
    {
        InitializeComponent();
        _teacher = teacher;
    }
}