﻿using System;
using System.Collections.Generic;

namespace Dekanat;

public partial class Institute
{
    public int Id { get; set; }

    public string Address { get; set; } = null!;

    public string Name { get; set; } = null!;

    public virtual ICollection<Department> Departments { get; } = new List<Department>();
}
