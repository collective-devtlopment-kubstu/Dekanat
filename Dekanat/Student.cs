﻿using System;
using System.Collections.Generic;

namespace Dekanat;

public partial class Student
{
    public int Id { get; set; }

    public int? IdGroup { get; set; }

    public string Surname { get; set; } = null!;

    public string Name { get; set; } = null!;

    public string? Patronymic { get; set; }

    public string PasData { get; set; } = null!;

    public string Telephone { get; set; } = null!;

    public string Email { get; set; } = null!;

    public string Password { get; set; } = null!;

    public virtual Group? IdGroupNavigation { get; set; }
}
