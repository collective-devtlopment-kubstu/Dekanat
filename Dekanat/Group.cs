﻿using System;
using System.Collections.Generic;

namespace Dekanat;

public partial class Group
{
    public int Id { get; set; }

    public int? IdSpecialization { get; set; }

    public string Name { get; set; } = null!;

    public virtual ICollection<GroupSubject> GroupSubjects { get; } = new List<GroupSubject>();

    public virtual Specialization? IdSpecializationNavigation { get; set; }

    public virtual ICollection<Student> Students { get; } = new List<Student>();
}
