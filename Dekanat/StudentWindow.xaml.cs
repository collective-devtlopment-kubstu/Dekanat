using System.Windows;

namespace Dekanat;

public partial class StudentWindow : Window
{
    private Student _student;
    public StudentWindow(Student student)
    {
        InitializeComponent();
        _student = student;
    }
}