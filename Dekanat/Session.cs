﻿using System;
using System.Collections.Generic;

namespace Dekanat;

public partial class Session
{
    public int? IdStudent { get; set; }

    public int? IdSubject { get; set; }

    public DateOnly Date { get; set; }

    public string Assessment { get; set; } = null!;

    public virtual Student? IdStudentNavigation { get; set; }

    public virtual Subject? IdSubjectNavigation { get; set; }
}
