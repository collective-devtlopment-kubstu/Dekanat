﻿using System;
using System.Collections.Generic;

namespace Dekanat;

public partial class Subject
{
    public int Id { get; set; }

    public string Name { get; set; } = null!;

    public string Type { get; set; } = null!;

    public string? Description { get; set; }

    public virtual ICollection<GroupSubject> GroupSubjects { get; } = new List<GroupSubject>();
}
