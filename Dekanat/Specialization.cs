﻿using System;
using System.Collections.Generic;

namespace Dekanat;

public partial class Specialization
{
    public int Id { get; set; }

    public int? IdDepartment { get; set; }

    public string Name { get; set; } = null!;

    public virtual ICollection<Group> Groups { get; } = new List<Group>();

    public virtual Department? IdDepartmentNavigation { get; set; }
}
